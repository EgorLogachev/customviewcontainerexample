package com.logachev.customviewexample1;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Yegor on 7/22/17.
 */

public class HorizontalLayout extends ViewGroup {

    int sumWeight;

    public HorizontalLayout(Context context) {
        super(context);
    }

    public HorizontalLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        getParamsFromAttributes(context, attrs);
    }

    public HorizontalLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getParamsFromAttributes(context, attrs);
    }

    private void getParamsFromAttributes(Context context, AttributeSet attributeSet) {
        /**
         * Получаем параметр аттрибута sumWeight указаного в xml разметке
         */
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.HorizontalLayout);
        sumWeight = typedArray.getInt(R.styleable.HorizontalLayout_sumWeight, -1);
        typedArray.recycle();
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {
        /**
         * Рассполагаем каждого ребенка обного за другим в пределах родителя слева направо
         */
        for (int n = 0; n < getChildCount(); n++) {
            View child = getChildAt(n);
            int left = 0;
            int top = 0;
            int right;
            int bottom;
            if (n != 0) {
                /**
                 * Если это не первый ребенок, то его левая координата будет соответствовать правой координате предидущего.
                 */
                View preChild = getChildAt(n - 1);
                left = preChild.getRight();
            }
            bottom = child.getMeasuredHeight();
            right = left + child.getMeasuredWidth();
            child.layout(left, top, right, bottom);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        /**
         * Образмериваем HorizontalLayout вызывая super
         */
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        if (sumWeight == -1) {
            /**
             * считаем sumWeight в случае если не задан
             */
            calculateWeightSum();
        }
        for (int i = 0; i < getChildCount(); i++) {
            /**
             * образмериваем всех детей в соответствии с параметром weight
             */
            View child = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) child.getLayoutParams();
            int childWidth = layoutParams.weight * width / sumWeight;
            child.measure(MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.EXACTLY), heightMeasureSpec);
        }
    }

    private void calculateWeightSum() {
        sumWeight = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            sumWeight += ((LayoutParams) child.getLayoutParams()).weight;
        }
    }

    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(p);
    }


    protected LayoutParams generateLayoutParams(int weight) {
        LayoutParams layoutParams = new LayoutParams(0, 0);
        layoutParams.weight = weight;
        return layoutParams;
    }

    /**
     * Этот метод используется системно при отрисовке экрана
     */
    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(new LayoutParams(0, 0));
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {

        int weight;

        public LayoutParams(Context c, AttributeSet attrs) {
            /**
             * Получаем параметр аттрибута weight указаного в xml разметке
             */
            super(c, attrs);
            TypedArray typedArray = c.obtainStyledAttributes(attrs, R.styleable.HorizontalLayout);
            weight = typedArray.getInt(R.styleable.HorizontalLayout_weight, 1);
            typedArray.recycle();
        }

        public LayoutParams(int width, int height) {
            super(width, height);
            weight = 1;
        }

        public LayoutParams(ViewGroup.LayoutParams source) {
            super(source);
            weight = 1;
        }
    }
}
